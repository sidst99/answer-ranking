import os
import glob
import collections
import pickle

from keras.utils.generic_utils import Progbar

from models.errorfunctions import rmse
from stackexchangedata.features.featuresets import featuresource2labels
from utils.reporting import *
from utils.generic import GeneratorLen, NormalizableCounter, list_or_single2list, chunkstring
from utils.settings import data_names, overview_files_considered
from utils.stats import mean_and_std, mean


class ModelEvaluator(object):
    def __init__(self, model, info_source, feature_source):
        self.model = model
        self.info_source = info_source
        self.data = info_source.get_corresponding_data_object_source()
        self.feature_source = feature_source

    def get_train_rmse(self):
        prediction = self.model.predict(self.feature_source.X_train).reshape(-1, 1)
        truth = self.feature_source.y_train
        return rmse(prediction, truth)

    def get_test_rmse(self):
        prediction = self.model.predict(self.feature_source.X_test).reshape(-1, 1)
        truth = self.feature_source.y_test
        return rmse(prediction, truth)

    def get_rmse(self, question_id):
        features = self.feature_source.get_features_by_parent_id(question_id)
        truths = self.feature_source.get_truths_by_parent_id(question_id)
        predictions = self.model.predict(features).reshape(-1, 1)
        return rmse(predictions, truths)

    def get_residuals(self):
        prediction = self.model.predict(self.feature_source.get_X()).reshape(-1, 1)
        truth = self.feature_source.get_y()
        return prediction - truth

    def compose_report_path(self, additional_tag=None):
        name_components = ["report"]
        name_components.append(self.info_source.parent_dir)
        name_components.append(self.model.name)
        name_components.append(self.feature_source.type)
        if additional_tag is not None:
            name_components.append(additional_tag)
        report_name = "_".join(name_components)
        data_dir = os.path.dirname(self.info_source.db_path)
        return os.path.join(data_dir, report_name)

    def predicted_answer_sets(self, question_ids):
        answer_sets = self.data.get_answer_sets(question_ids)
        for answer_set in answer_sets:
            answer_set.predict_scores(self.model, self.feature_source)
            yield answer_set

    def calculate_report_content(self, question_ids):
        report_content = ReportContent()
        answer_sets = GeneratorLen(self.predicted_answer_sets(question_ids), len(question_ids))
        report_content.add_answer_sets(answer_sets)
        report_content.add_model_info(self.model)
        report_content.add_overview()
        report_content.add_features_info(self.feature_source)
        return report_content

    def create_train_and_test_reports(self, min_answers, additional_tag=""):
        train_questions, test_questions = self.feature_source.get_reporting_question_ids(min_answers)
        self.create_report(train_questions, tag="train"+additional_tag)
        self.create_report(test_questions, tag="test"+additional_tag)
        self.save_dot_file()

    def list_train_and_test_files(self, additional_tag=""):
        file_types = [".pdf", ".json"]
        paths = []
        for t in file_types:
            paths.append(self.compose_report_path("train")+t+additional_tag)
            paths.append(self.compose_report_path("test")+t+additional_tag)
        return paths

    def create_report(self, question_ids, show_plots=False, tag=None):
        content = self.calculate_report_content(question_ids)
        self.save_pdf_report(content, show_plots, additional_name_tag=tag)
        self.save_txt_report(content, additional_name_tag=tag)

    def save_pdf_report(self, report_content, show=True, additional_name_tag=None):
        set_plot_fontsize(12)
        figures = []
        figures.extend(self.compose_common_figures(report_content))
        figures.extend(self.compose_model_specific_figures(report_content))
        path = self.compose_report_path(additional_name_tag) + ".pdf"
        save_as_pdf(figures, path)

    def compose_common_figures(self, report_content):
        s = "Kendall's Tau per question "
        return [
            plot_text("Overview", report_content["overview"], show=False),
            plot_histogram("RMSE per question", report_content["rmse"], maxrange=[-1,1], show=False),
            plot_normalized_histogram("NDCG per question", report_content["ndcg"], report_content["random_ndcg"], maxrange=[0, 1], show=False, cumulative=True),
            plot_normalized_histogram("%s (vs. y)" % s, report_content["tau_y"], report_content["random_kendall"], maxrange=[-1,1], show=False, cumulative=True),
            plot_normalized_histogram("%s (vs. score)" % s, report_content["tau_score"], report_content["random_kendall"], maxrange=[-1,1], show=False, cumulative=True),
            plot_histogram("%s (score vs. relative score)" % s, report_content["tau_base"], maxrange=[-1,1], show=False, cumulative=True),
        ]

    def compose_model_specific_figures(self, report_content):
        figures = []
        if report_content["feature_importances"] is not None:
            feature_importances = report_content["feature_importances"]
            feature_labels = report_content["feature_labels"]
            figures.append(plot_hbars("Feature Importances", feature_importances, feature_labels, show=False))
        return figures

    def save_txt_report(self, report_content, additional_name_tag=None):
        path = self.compose_report_path(additional_name_tag) + ".json"
        save_as_json(report_content.report_content, path)

    def save_dot_file(self, additional_name_tag=None):
        path = self.compose_report_path(additional_name_tag) + ".dot"
        save_dot(self.model, path)


class ReportContent(object):
    def __init__(self):
        self.report_content = {}
        self["rmse"] = list()
        self["ndcg"] = list()
        self["tau_y"] = list()
        self["tau_score"] = list()
        self["tau_base"] = list()
        self["nb_answers"] = list()
        self["recall"] = list()
        self["random_ndcg"] = list()
        self["random_kendall"] = list()
        self["random_recall"] = list()
        self.model_type = None
        self["feature_importances"] = None
        self["overview"] = None
        self["intercept"] = None
        self["model_params"] = None
        self["best_params"] = None

    def __setitem__(self, key, value):
        self.report_content[key] = value

    def __getitem__(self, item):
        return self.report_content[item]

    def add_answer_sets(self, predicted_answer_sets):
        progress_bar = Progbar(len(predicted_answer_sets))
        for answer_set in predicted_answer_sets:
            self.add_answer_set_metrics(answer_set)
            progress_bar.add(1)

    def add_answer_set_metrics(self, predicted_answer_set):
        self["rmse"].append(predicted_answer_set.get_rmse())
        self["tau_y"].append(predicted_answer_set.get_kendalls_tau("predicted_y", "ground_truths"))
        self["tau_score"].append(predicted_answer_set.get_kendalls_tau("predicted_y", "scores"))
        self["tau_base"].append(predicted_answer_set.get_kendalls_tau("scores", "ground_truths"))
        self["recall"].append(int(predicted_answer_set.same_best_answer("predicted_y", "ground_truths")))
        self["ndcg"].append(predicted_answer_set.ndcg())
        self["nb_answers"].append(len(predicted_answer_set))
        self["random_ndcg"].append(predicted_answer_set.ndcg("random"))
        self["random_kendall"].append(predicted_answer_set.get_kendalls_tau("random", "ground_truths"))
        self["random_recall"].append(1.0/len(predicted_answer_set))

    def add_model_info(self, model):
        self["feature_importances"] = model.feature_importances()
        self["model_params"] = model.params()
        self["best_params"] = model.best_params()
        self["intercept"] = model.intercept()

    def add_features_info(self, feature_source):
        self["feature_labels"] = featuresource2labels(feature_source)
        self["overview"].append("Features used: %s" % feature_source.type)

    def add_overview(self):
        self["overview"] = [""]
        self["overview"].append("Number of questions considered: %s" % len(self["rmse"]))
        try:
            minimum = min(self["nb_answers"])
            maximum = max(self["nb_answers"])
        except ValueError:
            minimum = "n/a"
            maximum = "n/a"
        self["overview"].append("Min/Max #answers for a question: %s / %s" % (minimum, maximum))
        self["overview"].append("Recall: %.3f  (random ordering would yield %.3f)" % (mean(self["recall"]), mean(self["random_recall"])))
        self["overview"].append("Avg. NDCG: %.3f (random ordering would yield %.3f)" % (mean(self["ndcg"]), mean(self["random_ndcg"])))
        self["overview"].append("Avg. Tau_y / Tau_score: %.3f / %.3f (random ordering would yield %.3f)" % (mean(self["tau_y"]), mean(self["tau_score"]), mean(self["random_kendall"])))
        if self["best_params"] is not None:
            self["overview"].append("Best params: %s" % self["best_params"])
        if self["intercept"] is not None:
            self["overview"].append("Intercept: %s" % self["intercept"])
        self["overview"].extend(chunkstring("Model params: %s" % str(self["model_params"]), chunk_length=90))


class Overviewer(object):
    """Providing methods to create summary views of a set of data directories

    It's main data structure is self.reports. A data_dir->report_path->report_content dict
    """

    def __init__(self, data_dirs):
        self.data_dirs = list_or_single2list(data_dirs)
        self.reports = {}
        self.load_reports()

    def load_reports(self):
        for d in self.data_dirs:
            self.reports[d] = {}
            search_path = os.path.join(d, overview_files_considered)
            for r in glob.glob(search_path):
                self.reports[d][r] = load_json(r)

    def compose_multibar_groups(self, field):
        data_groups = {}
        for data_dir in self.reports:
            group_name = self.data_group_name(data_dir)
            data_groups[group_name] = {}
            for report in self.reports[data_dir]:
                report_name = self.report_name(report)
                data = self.reports[data_dir][report].get(field, None)
                data_groups[group_name][report_name] = mean_and_std(data)
            data_groups[group_name] = collections.OrderedDict(sorted(data_groups[group_name].items()))
        return data_groups

    def compose_boxplot_series(self, field):
        data_series = {}
        for data_dir in self.reports:
            for report in self.reports[data_dir]:
                group_name = self.data_group_name(data_dir)
                report_name = self.report_name(report)
                data = self.reports[data_dir][report].get(field, None)
                data_series[group_name+"."+report_name] = data
        return collections.OrderedDict(sorted(data_series.items()))

    def compose_means(self, field):
        means = []
        for data_dir in self.reports:
            for report in self.reports[data_dir]:
                means.append(np.mean(self.reports[data_dir][report][field]))
        return means

    def data_group_name(self, data_dir):
        return data_names[os.path.dirname(data_dir)].replace("server-test-", "")

    @staticmethod
    def report_name(report_path):
        return "_".join(os.path.basename(report_path).replace(".json", "").replace("_test", "").split("_")[2:])

    def compose_figures(self):
        rmse_limits = [0.2, 0.4]
        tau_y_limits = [-1.1, 1.1]
        unit_limits = [0, 1]
        rmse_means = self.compose_means("rmse")
        figures = [
            plot_multibar("RMSE", self.compose_multibar_groups("rmse"), show=False),
            plot_multibar("NDCG", self.compose_multibar_groups("ndcg"), xlim=[0.8, 0.9], show=False, show_xerr=False),
            plot_multibar("recall of best answer", self.compose_multibar_groups("recall"), xlim=[0.4, 0.6], show=False, show_xerr=False),
            plot_multibar("tau_y", self.compose_multibar_groups("tau_y"), xlim=[0.4, 0.7], show=False, show_xerr=False),
            plot_boxplot("tau_y", self.compose_boxplot_series("tau_y"), xlim=tau_y_limits, show=False),
            plot_scatter("RMSE vs. tau_y", rmse_means, self.compose_means("tau_y"), rmse_means, self.compose_means("ndcg"), xlim=rmse_limits, ylim=unit_limits, show=False),
            plot_multibar("nb_answers", self.compose_multibar_groups("nb_answers"), show=False)
        ]
        return figures

    def create_overview_pdf(self, path="."):
        save_as_pdf(self.compose_figures(), path)