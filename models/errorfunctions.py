import numpy
from utils.generic import sign, NormalizableCounter
import itertools
import collections


def mse(v1, v2=None):
    v1 = numpy.array(v1)
    if v2 is None:
        return numpy.sum(v1**2)/v1.size
    else:
        v2 = numpy.array(v2)
        return numpy.sum((v1 - v2)**2)/v1.size


def rmse(v1, v2=None):
    if None in v1:
        return None
    if v2 is None:
        return numpy.sqrt(mse(v1))
    else:
        if v1.shape != v2.shape:
            raise ValueError("rmse(v1, v2): shapes of v1 and v2 do not match: %s vs. %s" % (str(v1.shape), str(v2.shape)))
        return numpy.sqrt(mse(v1, v2))


def excess_concordant_pairs(ranking):
    count = 0
    for i in range(1, len(ranking)):
        for j in range(0, i):
            concordant = sign(i - j)*sign(ranking[i] - ranking[j])
            # print "comparing pos %s: %s vs pos %s: %s ---> %s" % (i, ranking[i], j, ranking[j], concordant)
            count = count + concordant
    return count


def kendalls_tau(ranking):
    """https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient"""
    n = len(ranking)
    if n < 2:
        return 1
    else:
        total_number_of_pairs = n*(n-1)/2
        return float(excess_concordant_pairs(ranking))/total_number_of_pairs


def random_kendall_distribution(weighted_lengths):
    """
    :param weighted_lengths: dict of (lengths -> importance) pairs
    :return:
    """
    if weighted_lengths == {}:
        return []
    summed_weights = sum(weighted_lengths.values())
    if not 0.999 < summed_weights < 1.001:
        raise ValueError("Weights do not add up to 1, but sum to %s" % summed_weights)
    total = NormalizableCounter({}, 0)
    for length in weighted_lengths:
        l = range(length)
        permutations_of_l = list(itertools.permutations(l))
        taus_of_permutations = [kendalls_tau(p) for p in permutations_of_l]
        tau_counts = NormalizableCounter(taus_of_permutations)
        tau_counts = tau_counts.normalize()
        weight = weighted_lengths[length]
        total += tau_counts.scale(weight)
    return total.inflate()
