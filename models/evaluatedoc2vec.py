import itertools
from keras.utils.generic_utils import Progbar
import numpy

from utils.generic import NormalizableCounter, list_or_single2list


class Dov2VecModelEvaluator(object):
    def __init__(self, doc2vec_model):
        self.model = doc2vec_model
        self.show_progress = True

    def compute_model_accuracy(self, questions, answer_choices={}):
        correct = []
        incorrect = []
        if self.show_progress:
            print "Evaluating accuracy..."
        progress_bar = Progbar(len(questions))
        for i, q in enumerate(questions):
            possible_answers = answer_choices.get(q.split()[2], None)
            if self.model_answers_correctly(q, possible_answers=possible_answers):
                correct.append(q)
            else:
                incorrect.append(q)
            if (i%10 == 0 or i == len(questions)-1) and self.show_progress and len(questions) > 10:
                progress_bar.update(i)
        return {"correct": correct, "incorrect": incorrect}

    def model_answers_correctly(self, question, possible_answers=None):
        """Return True or False depending if the doc2vec model answers the question correctl"""
        a, b, c, d = question.split()
        positive = [b, c]
        negative = [a]
        if possible_answers:
            similar = self.most_similar_among_docs(possible_answers, positive=positive, negative=negative)
            return d == similar[0][1]
        else:
            similar = self.model.docvecs.most_similar(positive=positive, negative=negative)
            similar = [tag for (tag, similarity) in similar]
            return (d in similar)

    def most_similar_among_docs(self, docs=[], positive=[], negative=[]):
        from gensim import matutils
        from numpy import array as array, float32 as REAL, dot, vstack
        dv = self.model.docvecs
        dv.init_sims()

        if isinstance(positive, (str, int)) and not negative:
            # allow calls like most_similar('dog'), as a shorthand for most_similar(['dog'])
            positive = [positive]

        positive = [(doc, 1.0) for doc in positive]
        negative = [(doc, -1.0) for doc in negative]

        mean = []
        for doc, weight in positive + negative:
            mean.append(weight * dv.doctag_syn0norm[dv._int_index(doc)])
        mean = matutils.unitvec(array(mean).mean(axis=0)).astype(REAL)

        vectors = vstack(dv.doctag_syn0norm[dv._int_index(doc)] for doc in docs).astype(REAL)
        dists = dot(vectors, mean)

        return sorted(zip(dists, docs), reverse=True)

    def mean_and_std_of_question_answer_similarities(self, question_answers):
        means = []
        standard_deviations = []
        for q, answers in question_answers.items():
            sims = [self.model.docvecs.n_similarity([q], [a]) for a in answers]
            means.append(numpy.mean(sims))
            standard_deviations.append(numpy.std(sims))
        return means, standard_deviations

    def question_answer_similarities(self, question_answers):
        similarities = []
        for q, answers in question_answers.items():
            answers = list_or_single2list(answers)
            similarities.extend([self.model.docvecs.n_similarity([q], [a]) for a in answers])
        return similarities

    def random_guess_percent_right_answer(self, question_answers):
        normalized_counts = NormalizableCounter([len(answers) for answers in question_answers.values()]).normalize()
        s = 0
        for nb_answers, fraction in normalized_counts.items():
            s+=1.0/nb_answers*fraction
        return s


class QuestionGenerator(object):
    def __init__(self, info_source):
        self.info_source = info_source
        self.data_source = info_source.get_corresponding_data_object_source()
        self.generated_questions = []
        self.show_progress = True
        self.answer_choices = {}

    def save_questions(self):
        pass

    def get_questions(self):
        return self.generated_questions

    def compose_cross_questions(self, min_answers=10):
        pairs = self.compose_question_answer_pairs(min_answers)
        pairs = map(" ".join, pairs)
        pair_pairs = list(itertools.permutations(pairs, 2))
        self.generated_questions = [" ".join(p) for p in pair_pairs]

    def compose_local_cross_questions(self, doc2vec_model, min_answers=10):
        pairs = self.compose_question_answer_pairs(min_answers)
        pairs = dict(pairs)
        pair_pairs = []
        if self.show_progress:
            print "Composing local cross questions, checking which pair combos are valid..."
        progress_bar = Progbar(len(pairs))
        for p in pairs:
            neighbouring_questions = [t for t, s in doc2vec_model.docvecs.most_similar(p) if "QUESTION_" in t]
            most_similar_pairs = [(q, pairs[q]) for q in neighbouring_questions if pairs.has_key(q)]
            for similar_pair in most_similar_pairs:
                pair_pairs.append(((p, pairs[p]), similar_pair))
            progress_bar.add(1)
        self.generated_questions = ["%s %s" % (" ".join(p[0]), " ".join(p[1])) for p in pair_pairs]

    def compose_general_questions(self, min_answers=10):
        pairs = self.compose_question_answer_pairs(min_answers)
        self.generated_questions = ["QUESTION ANSWER %s" % " ".join(p) for p in pairs]

    def compose_question_answer_pairs(self, min_answers):
        question_ids = self.info_source.get_ids_of_questions_with_at_least_n_answers(min_answers)
        pairs = []
        if self.show_progress:
            print "Composing question-answer pairs..."
        progress_bar = Progbar(len(question_ids))
        answer_sets = self.data_source.get_answer_sets(question_ids)
        for i, answer_set in enumerate(answer_sets):
            pair = self.generate_question_answer_pair(answer_set)
            pairs.append(pair)
            if (i%10 == 0 or i == len(answer_sets)-1) and self.show_progress and len(answer_sets) > 10:
                progress_bar.update(i)
        return pairs

    def generate_question_answer_pair(self, answer_set):
        question_tag = "QUESTION_%s" % answer_set.parent_id
        answer_set.sort_by("ground_truths")
        answer_tag = answer_set.get("tags")[0]
        self.answer_choices[question_tag] = answer_set.get("tags")
        return question_tag, answer_tag