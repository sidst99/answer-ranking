import numpy as np

from utils.reporting import *


class ReportToolbox():
    def __init__(self, info_source):
        self.source = info_source

    def print_summary(self):
        print "Total number of questions: %s" % self.source.nb_questions()
        print "Total number of answers: %s" % self.source.nb_answers()

    def plot_answer_lengths(self):
        answer_lengths = np.array(self.source.get_answer_lengths())
        plot_log_histogram("Answer lengths", answer_lengths)

    def plot_answers_per_user(self):
        answers_per_user = np.array(self.source.get_answers_per_user())
        plot_log_histogram("Answers per user", answers_per_user)

    def plot_answers_per_question(self):
        answers_per_question = np.array(self.source.get_answers_per_question())
        plot_log_histogram("Answers per question", answers_per_question)

    def plot_answer_timings(self, answer_number):
        answer_timings = np.array(self.source.get_answer_timings(answer_number))
        plot_log_histogram("Time until answer number %s (Minutes)" % answer_number, answer_timings)

    def plot_answer_score(self):
        answer_scores = np.array(self.source.get_answer_scores())
        plot_semilogy_histogram("Answer scores", answer_scores)

    def plot_comments_per_answer(self):
        comment_counts = np.array(self.source.get_comments_per_answer())
        m = np.mean(comment_counts)
        std = np.std(comment_counts)
        plot_histogram("Comments per answer (mean = %1.2f, std = %1.2f)" % (m, std), comment_counts)

    def plot_viewcount_vs_total_votes(self, window_size=100, xmax=100000):
        vc, tv = self.source.get_viewcount_vs_total_votes()
        tv = np.convolve(tv, np.ones(window_size)/window_size, 'same')
        plot_scatter("View count of question vs. (avg.) Number of votes per answer", vc, tv, xlim=[0, xmax], ylim=[0,25])

