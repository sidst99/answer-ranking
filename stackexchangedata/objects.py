from utils.generic import decouple_sum_difference, none2zero, rank_list, mmr_sorted
from utils.sqlite import tuple2single, row_and_description2dict
from math import log
from random import random

from models.errorfunctions import rmse, kendalls_tau


class Element(object):
    """Database element baseclass

    Provides methods to initialize an object from the database or given a row
    from a database. Used for example by the Answer() class.
    """
    def __init__(self, row, db):
        self.db = db
        self.properties = row

    def __getitem__(self, item):
        return none2zero(self.properties[item])

    def __getattr__(self, item):
        item = item.translate(None, "_")
        try:
            return self.properties[item]
        except IndexError as e:
            m = "%s object has no attribute %s\n" % (type(self).__name__, item)
            m += "Available attributes are %s" % self.available_properties()
            raise IndexError(m)

    def available_properties(self):
        return self.properties.keys()

    def __str__(self):
        return "<%s object: Id %s>" % (type(self).__name__, self.properties["PostId"])


class ElementSet(object):
    def __init__(self, rows, db):
        self.db = db
        self.elements = self.init_elements(rows, db)

    def init_elements(self, rows, db):
        raise NotImplementedError("Subclasses need to implement _init_from_rows.")

    def __len__(self):
        if self.elements is None:
            return 0
        else:
            return len(self.elements)

    def __iter__(self):
        if self.elements is None:
            return
            yield
        for e in self.elements:
            yield e

    def __getitem__(self, item):
        return self.elements[item]

    def __str__(self):
        return ", ".join([str(e) for e in self])


class Comment(Element):
    pass


class CommentSet(ElementSet):
    def init_elements(self, rows, db):
        return [Comment(r, db) for r in rows]

    def get_scores(self):
        return [c["Score"] for c in self]

    def get_average_score(self):
        scores = self.get_scores()
        if len(scores) == 0:
            return 0
        else:
            return float(sum(scores))/len(scores)

    def get_nb_comments(self):
        return len(self)


class Answer(Element):
    def __init__(self, row, db):
        super(Answer, self).__init__(row, db)
        self.true_y = None
        self.predicted_y = None
        self.error = None
        self.cluster = None

    @property
    def id(self):
        return self["PostId"]

    @property
    def question_id(self):
        return self["ParentId"]

    @property
    def total_votes(self):
        return none2zero(self["Totalvotes"])

    def get_score(self):
        return self.score

    def get_corresponding_answer_set(self):
        return self.db.get_answer_set(self["ParentId"])

    def get_corresponding_answerer(self):
        return self.db.get_user(self["OwnerUserId"])

    def get_corresponding_question(self):
        return self.db.get_question(self["ParentId"])

    def get_corresponding_comments(self):
        return self.db.get_comment_set(self["PostId"])

    def predict_y(self, trained_model, feature_source):
        features = feature_source.get_features(self.id)
        self.true_y = feature_source.get_ground_truth(self.id)
        self.predicted_y = trained_model.predict(features)[0]
        self.error = self.true_y - self.predicted_y
        return self.predicted_y

    def get_predicted_score(self):
        if self.predicted_y is None:
            return None
        else:
            return int(self.predicted_y*(self.total_votes + 1))

    def get_relative_votes_received(self):
        answer_set = self.get_corresponding_answer_set()
        s = sum(answer_set.get("scores"))
        s = s if s is not 0 else 1
        return float(self.score)/s

    def get_arrival_number(self):
        answer_set = self.get_corresponding_answer_set()
        return sum([1 for id in answer_set.get("ids") if id <= self.id])

    def get_ground_truth(self):
        return float(self.score)/(self.total_votes + 1)

    @property
    def tag(self):
        return "ANSWER_%s" % self.id

    def set_cluster(self, cluster_id):
        self.cluster = cluster_id


class AnswerSet(ElementSet):
    def __init__(self, row, db):
        super(AnswerSet, self).__init__(row, db)
        self.sort_by("scores")

    def init_elements(self, rows, db):
        return [Answer(r, self) for r in rows]

    def predict_scores(self, trained_model, feature_source):
        for a in self.elements:
            a.predict_y(trained_model, feature_source)
        self.sort_by("predicted_y")

    def compute_mmr_ranks(self, doc2vec_similarity_function):
        docs = set(self.elements)
        q = None
        similarity1 = lambda a, q: a.predicted_y
        similarity2 = lambda a1, a2: doc2vec_similarity_function([a1.tag], [a2.tag])
        if similarity1(self[0], None) is None:
            raise ValueError("Scores need to be predicted on answer set to rank by MMR")
        self.mmr_ranks = mmr_sorted(docs, q, 0.5, similarity1, similarity2)
        for a in self.elements:
            a.mmr_rank = self.mmr_ranks[a]

    keys = {
        "ids": lambda x: x.id,
        "scores": lambda x: x.score,
        "ground_truths": lambda x: x.get_ground_truth(),
        "predicted_y": lambda x: x.predicted_y,
        "errors": lambda x: x.error,
        "tags": lambda x: x.tag,
        "mmr_ranks": lambda x: x.mmr_rank,
        "random": lambda x: random()
    }

    def get(self, k):
        return [self.keys[k](a) for a in self]

    def get_rmse(self):
        errors = self.get("errors")
        return rmse(errors)

    def sort_by(self, k):
        self.elements.sort(key=self.keys["random"])
        reverse = False if k == "ids" or k == "mmr_rank" else True
        self.elements.sort(key=self.keys[k], reverse=reverse)
        self.sorted_by = k

    def rank_by(self, k):
        return rank_list([self.keys[k](a) for a in self])

    def get_kendalls_tau(self, key1, key2):
        key_at_start = self.sorted_by
        self.sort_by(key1)
        t = kendalls_tau(self.rank_by(key2))
        self.sort_by(key_at_start)
        return t

    def same_best_answer(self, key1, key2):
        if len(self) == 0:
            return True
        key_at_start = self.sorted_by
        self.sort_by(key1)
        best_answer1 = self[0]
        self.sort_by(key2)
        best_answer2 = self[0]
        self.sort_by(key_at_start)
        return best_answer1 == best_answer2

    def ndcg(self, key1="predicted_y", relevance_key="scores"):
        """Compute normalized discounted cumulative gain (NDCG) of the answers ordering

        NDCG works as described in https://en.wikipedia.org/wiki/Discounted_cumulative_gain

        :param key1: sort the AnswerSet by this key before calculating the NDGC
        :param rel: the key giving the relevance of an answer with respect to a question
        :return: a score between 0 and 1
        """
        if len(self) == 0:
            return 0
        key_at_start = self.sorted_by
        self.sort_by(key1)
        relevance_function = self.keys[relevance_key]
        dcg = self.dcg(relevance_function)
        self.sort_by(relevance_key)
        idcg = self.dcg(relevance_function)
        self.sort_by(key_at_start)
        if idcg == 0:
            return 0
        else:
            return float(dcg)/idcg

    def dcg(self, rel, return_list=False):
        s = []
        for i, a in enumerate(self.elements, start=1):
            d = float((2**rel(a)-1))/log(i+1)
            s.append(d)
        if return_list:
            return s
        else:
            return sum(s)

    @property
    def parent_id(self):
        try:
            return self.elements[0].parent_id
        except IndexError:
            return None

    def set_clusters(self, cluster_ids):
        if len(cluster_ids) != len(self):
            raise ValueError
        self.sort_by("ids")
        for i, answer in enumerate(self.elements, start = 0):
            answer.set_cluster(cluster_ids[i])

    def evaluation_metric(self, key="ground_truths", constant=0.5):
        """
        Compute an evaluation metric based on a combination of the diversity and the relevance metrics.
        In depth description : http://ir.ii.uam.es/predict/pubs/recsys11-vargas.pdf

        :param key: sort the AnswerSet by this key before calculating the metric
        :param constant: the discounting factor used in calculating the metric
        :return: a tuple of the diversity, the relevance and the evaluation metrics
        """
        self.sort_by(key)
        diversity_metric = 0
        relevance_metric = 0
        evaluation_metric = 0
        L = len(self) # is the length of the answer set
        for i, answer in enumerate(self.elements, start = 1):
            # ni is the number of higher ranked answers in the same cluster wrt any particular answer:
            ni = len(self.higher_rank_similar_answers(answer))
            diversity_metric += (L - i + 1) * (constant ** ni)
            relevance_metric += answer.get_ground_truth()
            evaluation_metric += (L - i + 1) * (constant ** ni) * answer.get_ground_truth()
        return (diversity_metric, relevance_metric, evaluation_metric)

    def higher_rank_similar_answers(self, answer, key="ground_truths"):
        key_at_start = self.sorted_by
        self.sort_by(key)
        higher_ranked_answers = []
        for a in self.elements:
            if a.id == answer.id:
                break
            if a.cluster == answer.cluster:
                higher_ranked_answers.append(a)
        self.sort_by(key_at_start)
        return higher_ranked_answers

class Question(Element):
    def corresponding_answer_ids(self):
        query = "SELECT Id FROM posts WHERE ParentId == %s AND PostTypeId == 2" % self.question_id
        return map(tuple2single, list(self.db.execute(query)))

    def get_corresponding_asker(self):
        return self.db.get_user(self.owner_user_id)


class QuestionSet(ElementSet):
    def init_elements(self, rows, db):
        return [Question(r, self) for r in rows]


class User(Element):
    @property
    def upvotes_received(self):
        return decouple_sum_difference(self.votes_received, self.score_received)[0]

    @property
    def downvotes_received(self):
        return decouple_sum_difference(self.votes_received, self.score_received)[1]

    @property
    def votes_received(self):
        return none2zero(self["VotesReceived"])

    @property
    def score_received(self):
        return none2zero(self["ScoreReceived"])

    @property
    def answers_accepted(self):
        return none2zero(self["AnswersAccepted"])

    @property
    def answers_written(self):
        return none2zero(self["AnswersWritten"])

    @property
    def reports_received(self):
        return none2zero(self["ReportsReceived"])

    def get_corresponding_answers_received(self, question_selector=None):
        if self.id == 0:
            return []
        else:
            return self.db.get_answers_given_to_user(self.id, question_selector)

    def get_corresponding_questions_asked(self, question_selector=None):
        if self.id == 0:
            return []
        else:
            return self.db.get_questions_asked_by_user(self.id, question_selector)


class ExtendedAnswer(Answer):
    @property
    def part_of_train_set(self):
        return none2zero(self["PartOfTrainSet"])