#!/usr/bin/python
import logging
import os
import sys
import inspect
import glob


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from gensim.models import Doc2Vec
from utils.settings import get_db, get_db_name_from_input, get_settings
from models.evaluatedoc2vec import Dov2VecModelEvaluator, QuestionGenerator
from stackexchangedata.db import InfoSource

program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
logging.root.setLevel(level=logging.INFO)
logger.info("running %s" % ' '.join(sys.argv))


dataset_name = get_db_name_from_input()
dataset_dir = get_db(dataset_name, "dir")
settings = get_settings(dataset_name)
min_answers = settings["report_min_answers"]
save_to = os.path.join(dataset_dir, "questioning_results_%s.txt" % dataset_name)


doc2vec_models = glob.glob(os.path.join(dataset_dir, "doc2vecmodel_*"))
doc2vec_models = [m for m in doc2vec_models if "syn" not in m]
print doc2vec_models
info_source = InfoSource(get_db(dataset_name))
question_generator = QuestionGenerator(info_source)
questions = {}
question_generator.compose_general_questions(min_answers=min_answers)
questions["general"] = question_generator.generated_questions


results = []
for m in doc2vec_models:
    print "Assessing model %s" % m
    model = Doc2Vec.load(m)

    question_generator.compose_local_cross_questions(model, min_answers=min_answers)
    questions["local"] = question_generator.generated_questions

    print "\nGenerated %s questions" % len(question_generator.generated_questions)
    if len(question_generator.generated_questions) == 0:
        continue
    evaluator = Dov2VecModelEvaluator(model)

    answer_choices = question_generator.answer_choices

    for t, ql in questions.items():
        a = evaluator.compute_model_accuracy(ql, answer_choices)
        percent_right = float(len(a["correct"]))/(len(a["incorrect"])+len(a["correct"]))
        result = "%s got %s of %s questions right (min. answers = %s)\n" % (m, percent_right, t, min_answers)
        print result
        results.append(result)

    random_score = evaluator.random_guess_percent_right_answer(answer_choices)
    results.append("A random guess would have produced %s\n" % random_score)

with open(save_to, mode='w') as fp:
    fp.writelines(results)