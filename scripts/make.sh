#!/usr/bin/env bash


SCRIPTS_DIR="/Users/dmeier/Dropbox/epfl-masterthesis/masterproject/scripts"
#SCRIPTS_DIR="/home/dmeier/data/code/scripts"

MAKEDB_SCRIPT="$SCRIPTS_DIR/makedb.py"
MAKEDOC2VEC_SCRIPT="$SCRIPTS_DIR/makedoc2vec.py"
MAKEFEATURES_SCRIPT="$SCRIPTS_DIR/makefeatures.py"
MAKEREPORT_SCRIPT="$SCRIPTS_DIR/makereport.py"
MAKEQUESTIONING_SCRIPT="$SCRIPTS_DIR/makequestioning.py"
MAKENGRAM_SCRIPT="$SCRIPTS_DIR/makengram.py"

DBS="beer"

#DBS="
#server-test-cooking
#server-test-electronics
#server-test-math
#server-test-physics
#server-test-stats
#server-test-english
#"

function makedb {
    echo "Creating sqlite db for $DB_NAME"
    python $MAKEDB_SCRIPT $DB_NAME
}

function makedoc2vec {
    echo "Creating doc2vec model for $DB_NAME"
    python $MAKEDOC2VEC_SCRIPT $DB_NAME
}

function makefeatures {
    echo "Calculating features for $DB_NAME"
    python $MAKEFEATURES_SCRIPT $DB_NAME
}

function makereport {
    echo "Creating report for $DB_NAME"
    python $MAKEREPORT_SCRIPT $DB_NAME
}

function makequestioning {
    echo "Creating questioning report for doc2vec model of $DB_NAME"
    python $MAKEQUESTIONING_SCRIPT $DB_NAME
}

function makengram {
    echo "Creating ngram models for $DB_NAME"
    python $MAKENGRAM_SCRIPT $DB_NAME
}


for DB_NAME in $DBS
do
    case "$1" in
        makedb)
            makedb
            ;;
        makedoc2vec)
            makedoc2vec
            ;;
        makefeatures)
            makefeatures
            ;;
        makereport)
            makereport
            ;;
        makequestioning)
            makequestioning
            ;;
        makengram)
            makengram
            ;;
        *)
            makedb
            makedoc2vec
            makengram
            makefeatures
            makereport
    esac
done
