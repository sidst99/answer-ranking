#!/usr/bin/python
import logging
import os
import sys
import inspect


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from utils.settings import get_db, get_db_name_from_input, get_settings


program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
logging.root.setLevel(level=logging.INFO)
logger.info("running %s" % ' '.join(sys.argv))

#configuration
dataset_name = get_db_name_from_input()
settings = get_settings(dataset_name)
nb_epochs = settings["doc2vec_training_epochs"]
size = settings["doc2vec_size"]

def train_and_save(dataset_name, nb_epochs, size, longname=False):
    datadir = os.path.dirname(get_db(dataset_name))
    savepath = os.path.join(datadir, "doc2vecmodel")
    if longname:
        savepath += "_%sepochs_size%s" % (nb_epochs, size)
    if not os.path.isfile(savepath):
        from gensim.models import Doc2Vec
        from stackexchangedata.db import DocumentSource
        documents = DocumentSource(get_db(dataset_name))
        model = Doc2Vec(size=size, window=8, min_count=5, workers=4, alpha=0.025, min_alpha=0.0025)
        model.build_vocab(documents)
        for epoch in range(nb_epochs):
            logger.info("starting epoch %s/%s" % (epoch+1, nb_epochs))
            model.train(documents)
        model.save(savepath)
    else:
        print "%s already exists, not executing makedoc2vec.py" % savepath


if type(size) == list:
    print "Creating doc2ec models with sizes %s" % size
    for s in size:
        train_and_save(dataset_name, nb_epochs, s, longname=True)
else:
    train_and_save(dataset_name, nb_epochs, size)
