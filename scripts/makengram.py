#!/usr/bin/python
import logging
import os
import sys
import inspect


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from utils.settings import get_db, get_db_name_from_input, get_settings
from stackexchangedata.db import DocumentSource
from utils.ngram import build_vocabulary, count_ngrams, NgramModel
from itertools import chain


program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
logging.root.setLevel(level=logging.INFO)
logger.info("running %s" % ' '.join(sys.argv))

#configuration
dataset_name = get_db_name_from_input()
settings = get_settings(dataset_name)
sizes = settings["ngram_model_size"]


def train_and_save(dataset_name, size):
    datadir = os.path.dirname(get_db(dataset_name))
    savepath = os.path.join(datadir, model_name(size))
    if not os.path.isfile(savepath):
        print "Creating %s" % savepath
        documents = DocumentSource(get_db(dataset_name), split=False)
        print "Tuples generated..."
        chars = chain.from_iterable(documents)
        print "Chars generated..."
        vocab = build_vocabulary(1, chars)
        print "Vocab built..."
        counts = count_ngrams(size, vocab, documents)
        print "Counting done..."
        model = NgramModel(counts)
        model.save(savepath)
    else:
        print "%s already exists, not executing makedoc2vec.py" % savepath


def model_name(size):
    return "%s-grammodel" % size


if type(sizes) == list:
    print "Creating doc2ec models with sizes %s" % sizes
    for s in sizes:
        train_and_save(dataset_name, s)
elif type(sizes) == int:
    train_and_save(dataset_name, sizes)
else:
    print "Unknown format of sizes given for ngram model calculation"
