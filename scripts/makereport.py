#!/usr/bin/python
import os
import sys
import inspect
import logging


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


from models.featuresource import FeatureSource
from models.model import Model
from models.evaluate import ModelEvaluator
from stackexchangedata.db import InfoSource
from utils.settings import get_db_name_from_input, get_db, get_db_logfile, get_settings
from utils.files import any_file_exists


dataset_name = get_db_name_from_input()
db = get_db(dataset_name)
settings = get_settings(dataset_name)
counter = 1
total = len(settings["report_features_db"])*len(settings["report_model"])
for feature_type in settings["report_features_db"]:
    features_db = get_db(dataset_name, feature_type)
    logfile = get_db_logfile(dataset_name)
    logging.basicConfig(filename=logfile,level=logging.INFO)


    feature_source = FeatureSource(features_db)
    info_source = InfoSource(db)
    for model_type in settings["report_model"]:
        additional_tag = "" if not settings["train_by_score"] else "_score"
        print "Creating report %s/%s (features: %s, model: %s%s)..." % (counter, total, feature_type, model_type, additional_tag)
        counter += 1
        model = Model(model_type)
        if dataset_name not in ["beer"]:
            model.make_fancy()
        evaluator = ModelEvaluator(model, info_source, feature_source)

        if not any_file_exists(evaluator.list_train_and_test_files(additional_tag=additional_tag)):
            model.train(feature_source, settings["train_by_score"])
            evaluator.create_train_and_test_reports(settings["report_min_answers"], additional_tag=additional_tag)
        else:
            print "At least one of the following reports already exists, not executing makereport.py:"
            for f in evaluator.list_train_and_test_files():
                print f
