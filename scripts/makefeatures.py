#!/usr/bin/python
#script to take a stackoverflow database and create a corresponding feature database
import sqlite3
import logging
import os
import inspect
import sys


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from stackexchangedata.features.featuresets import get_feature_set
from utils.settings import get_db, get_db_name_from_input, get_db_logfile, get_settings


dataset_name = get_db_name_from_input()
settings = get_settings(dataset_name)
for featureset in settings["features_featureset"]:
    print "Creating featureset %s..." % featureset
    featureset = get_feature_set(featureset, dataset_name)


    source_db = get_db(dataset_name)
    target_db = get_db(dataset_name, featureset.name())
    logfile = get_db_logfile(dataset_name)
    logging.basicConfig(filename=logfile,level=logging.INFO)



    if not os.path.isfile(target_db):
        from keras.utils.generic_utils import Progbar
        from stackexchangedata.db import InfoSource, DatabaseConnection
        from stackexchangedata.objects import ExtendedAnswer
        logging.info("Connecting to %s" % (source_db))
        data_info_source = InfoSource(source_db)
        db_connection = DatabaseConnection(source_db)
        logging.info("Creating features in %s" % (target_db))
        output_db = sqlite3.connect(target_db)

        create_query = "CREATE TABLE features(AnswerId INTEGER PRIMARY KEY, ParentId, PartOfTrainSet, Score, Truth, Features)"
        output_db.execute(create_query)
        logging.info("Created table features()")

        try:
            total_entries = db_connection.execute("SELECT count(*) FROM extendedanswers").fetchone()[0]
            progbar = Progbar(total_entries)
            db_cursor = db_connection.execute("SELECT * FROM extendedanswers")
            for i, row in enumerate(db_cursor):
                extended_answer = ExtendedAnswer(row, db_connection)
                answer_id = extended_answer.post_id
                parent_id = extended_answer.parent_id
                owner_id = extended_answer.owner_user_id
                score = extended_answer.score
                truth = featureset.get_ground_truth(extended_answer)
                features = featureset.from_extended_answer(extended_answer)
                insert_query = "INSERT INTO features  (AnswerId, ParentId, Score, Truth, Features) VALUES (%s, %s, %s, %s, '%s')"\
                                                    % (answer_id, parent_id, score, truth, features)
                logging.info("Adding answer %s/%s: %s" % (i+1, total_entries, insert_query))
                output_db.execute(insert_query)
                progbar.update(i+1)
            output_db.commit()
            output_db.close()
        except KeyboardInterrupt:
            output_db.commit()
            output_db.close()
    else:
        print "%s already exists, not executing makefeatures.py" % target_db
