import datetime


def from_isotime(iso_timestamp):
    dt = datetime.datetime.strptime(iso_timestamp, "%Y-%m-%dT%H:%M:%S.%f")
    return dt


def timestamps2timedeltas(list_of_timestamps):
    list_of_timedeltas = []
    for i in xrange(0, len(list_of_timestamps)-1):
        iso_timestamp1 = list_of_timestamps[i]
        iso_timestamp2 = list_of_timestamps[i+1]
        delta = timedelta_in_minutes(iso_timestamp1, iso_timestamp2)
        list_of_timedeltas.append(delta)
    return list_of_timedeltas


def timedelta_in_minutes(iso_timestamp1, iso_timestamp2):
    t1 = from_isotime(iso_timestamp1)
    t2 = from_isotime(iso_timestamp2)
    return ((t2 - t1).total_seconds())/60