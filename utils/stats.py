import numpy


def mean_and_std(vector):
    if vector is None:
        return None
    vector = numpy.array(vector)
    return (vector.mean(), vector.std())


def mean(l):
    if l == []:
        print "Hack warning: returning 0.0 as mean() of an empty list"
        return 0.0
    return float(sum(l))/len(l)