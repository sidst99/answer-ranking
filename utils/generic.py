import collections


def decouple_sum_difference(sum, difference):
    if sum is None or difference is None:
        return None, None
    else:
        positive_part = (sum + difference) / 2
        negative_part = (sum - difference) / 2
    return positive_part, negative_part


def none2zero(v):
    if v is None:
        return 0
    else:
        return v


def rank_list(l):
    return [x + 1 for x, y in sorted(enumerate(l), key=lambda x: x[1], reverse=True)]


sign = lambda x: (1, -1)[x < 0]


def none_or_float2str(none_or_float):
    if none_or_float is None:
        return "None"
    else:
        return "%1.2f" % none_or_float


def list_or_single2list(e):
    if not type(e) is list:
        return [e]
    else:
        return e


class GeneratorLen(object):
    def __init__(self, gen, length):
        self.gen = gen
        self.length = length

    def __len__(self):
        return self.length

    def __iter__(self):
        return self.gen


class NormalizableCounter(collections.Counter):
    def __init__(self, e, counted_already=None):
        super(NormalizableCounter, self).__init__(e)
        if type(e) in [dict, collections.Counter]:
            if counted_already is None:
                raise ValueError(
                    "Initializing NomralizedCounter by %s requires to set counted_already on initiation" % type(e))
            self.counted = counted_already
        elif type(e) is list:
            self.counted = len(e)
        elif type(e) is NormalizableCounter:
            self.counted = e.counted

    def normalize(self):
        total = sum(self.values(), 0.0)
        if total != 0:
            self.scale(1.0 / total)
        return self

    def scale(self, factor):
        for key in self:
            self[key] *= factor
        return self

    def __add__(self, other):
        s = super(NormalizableCounter, self).__add__(other)
        my_count = self.counted
        other_count = other.counted
        return NormalizableCounter(s, my_count + other_count)

    def inflate(self):
        l = []
        for value, count in self.items():
            l.extend([value] * int(round(self.counted * count)))
        return l


def mmr_sorted(docs, q, lambda_, similarity1, similarity2):
    """Sort a list of docs by Maximal marginal relevance

    Performs maximal marginal relevance sorting on a set of
    documents as described by Carbonell and Goldstein (1998)
    in their paper "The Use of MMR, Diversity-Based Reranking
    for Reordering Documents and Producing Summaries"

    :param docs: a set of documents to be ranked by maximal marginal relevance
    :param q: query to which the documents are results
    :param lambda_: lambda parameter, a float between 0 and 1
    :param similarity1: sim_1 function. takes a doc and the query
            as an argument and computes their similarity
    :param similarity2: sim_2 function. takes two docs as arguments
            and computes their similarity score
    :return: a (document, mmr score) ordered dictionary of the docs
            given in the first argument, ordered my MMR
    """
    selected = collections.OrderedDict()
    while set(selected) != docs:
        remaining = docs - set(selected)
        mmr_score = lambda x: lambda_ * similarity1(x, q) - (1 - lambda_) * max(
            [similarity2(x, y) for y in set(selected) - {x}] or [0])
        next_selected = argmax(remaining, mmr_score)
        selected[next_selected] = len(selected)
    return selected


def argmax(keys, f):
    return max(keys, key=f)


def insert_newlines(string, every=64):
    return '\n'.join(string[i:i+every] for i in xrange(0, len(string), every))


def chunkstring(string, chunk_length):
    return (string[0+i:chunk_length+i] for i in range(0, len(string), chunk_length))