import os
import sys


def get_settings(dataset_name):
    if dataset_name == "beer":
        settings = {
            "doc2vec_training_epochs": 3,
            "doc2vec_size": 80,
            "features_featureset": ["halfofagichteinfeatures"],
            "report_features_db": ["halfofagichteinfeatures"],
            "report_model": ["linear_regression"],
            "report_min_answers": 3,
            "ngram_model_size": [1, 3],
            "train_by_score": False
        }
    else:
        settings = {
            "doc2vec_training_epochs": 10,
            "doc2vec_size": 100,
            "features_featureset": ["epfl1features"],
            "report_features_db": ["epfl6features", "epfl4features", "agichteinfeatures"],
            "report_model": ["decision_tree2",
                             "random_forestCV",
                             "svmCV",
                             "linear_regression",
                             "ridge_regression",
                             "ridge_regressionCV"
                             ],
            "report_min_answers": 3,
            "ngram_model_size": [1, 3],
            "train_by_score": False
        }
    return settings


overview_files_considered = "report_*_test.json"

data_dirs = {
    "beer": "/Users/dmeier/Dropbox/epfl-masterthesis/masterproject/fixtures",
    "beer-temp": "/tmp/answerrankingtest",
    "stats": "/Users/dmeier/Documents/Datasets/stats.stackexchange.com.2016-02-10",
    "server-test-cooking": "/home/dmeier/data/test/cooking.stackexchange.com",
    "server-test-electronics": "/home/dmeier/data/test/electronics.stackexchange.com",
    "server-test-math": "/home/dmeier/data/test/math.stackexchange.com",
    "server-test-physics": "/home/dmeier/data/test/physics.stackexchange.com",
    "server-test-stats": "/home/dmeier/data/test/stats.stackexchange.com",
    "server-test-english": "/home/dmeier/data/test/english.stackexchange.com",
    "server-stackoverflow": "/home/dmeier/data/experiment/stackoverflow.com",
    "stats_Siddharth": "F:/Project/stats.stackexchange.com",
    "engineering_Siddharth": "F:/Project/engineering.stackexchange.com"
}

data_names = {v: k for k, v in data_dirs.items()}

db_types = {
    "base": "so-dump.db",
    "agichteinfeatures": "agichtein-features.db",
    "halfofagichteinfeatures": "half-agichtein-features.db",
    "epfl1features": "epfl1-features.db",
    "epfl2features": "epfl2-features.db",
    "epfl3features": "epfl3-features.db",
    "epfl4features": "epfl4-features.db",
    "epfl5features": "epfl5-features.db",
    "epfl6features": "epfl6-features.db",
    "empty": "so-dump-empty.db",
    "doc2vec": "doc2vecmodel",
    "unigram": "1-grammodel",
    "trigram": "3-grammodel",
    "dir": ""
}


def get_db(name, type="base"):
    assert_valid_db_name(name)
    dir = data_dirs[name]
    name = db_types[type]
    return os.path.join(dir, name)


def get_db_logfile(name):
    dir = data_dirs.get(name)
    logname = "p.log"
    return os.path.join(dir, logname)


def get_db_name_from_input():
    """Reads the first command line argument and tries to interpret it as a db name
    By default (i.e. if no command line argument was given) it returns "beer"
    """
    if len(sys.argv) > 1:
        db_name = sys.argv[1]
    else:
        db_name = "beer"
    assert_valid_db_name(db_name)
    return db_name


def assert_valid_db_name(db_name):
    if db_name not in data_dirs:
        raise LookupError("No dataset named %s is not configured" % db_name)
    elif not os.path.isdir(data_dirs.get(db_name)):
        raise LookupError("No dataset found at %s" % data_dirs.get(db_name))
    else:
        return True


def yn_choice(message, default='y'):
    choices = 'Y/n' if default.lower() in ('y', 'yes') else 'y/N'
    choice = raw_input("%s (%s) " % (message, choices))
    values = ('y', 'yes', '') if default == 'y' else ('y', 'yes')
    return choice.strip().lower() in values