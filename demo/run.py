import sys
import time

from BaseHTTPServer import HTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
from urlparse import urlparse, parse_qs

from stackexchangedata.db import DataObjectSource
from demo.htmlcomposer import RankingHtmlComposer
from models.featuresource import FeatureSource
from models.model import ModelLibrary
from utils.settings import get_db

data_source = DataObjectSource(get_db("stats"))
feature_source = FeatureSource(get_db("stats", "epfl1features"))
trained_model = ModelLibrary().get_and_train_model("linear_regression", feature_source)


class RankingDemoHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        question_id = self.get_question_id()
        html_composer = RankingHtmlComposer(question_id, data_source)
        html = html_composer.compose_predicted_ranking_html(trained_model, feature_source)
        self.send(html)

    def send(self, html, code=200):
        self.send_response(code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(html.encode("utf-8"))

    def get_question_id(self):
        query_components = parse_qs(urlparse(self.path).query)
        given_question_id = query_components.get("question_id", ['1'])
        given_answer_id = query_components.get("answer_id")
        if given_answer_id is None:
            return given_question_id[0].encode("utf-8")
        else:
            answer_id = given_answer_id[0].encode("utf-8")
            return data_source.get_answer(answer_id).parent_id


if sys.argv[1:]:
    port = int(sys.argv[1])
else:
    port = 8000


def main():
    try:
        server = HTTPServer(('', port), RankingDemoHandler)
        address_info = server.socket.getsockname()
        print "Serving HTTP on", address_info[0], "port", address_info[1], "..."
        server.serve_forever()
    except KeyboardInterrupt:
        print '^C received, shutting down server'
        server.socket.close()

if __name__ == '__main__':
    main()