import os

from utils.generic import none_or_float2str


__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))


mathjax_header = """
                <script type="text/x-mathjax-config">
                    MathJax.Hub.Config({"HTML-CSS": { preferredFont: "TeX", availableFonts: ["STIX","TeX"], linebreaks: { automatic:true }, EqnChunk: (MathJax.Hub.Browser.isMobile ? 10 : 50) },
                                        tex2jax: { inlineMath: [ ["$", "$"], ["\\\\(","\\\\)"] ], displayMath: [ ["$$","$$"], ["\\[", "\\]"] ], processEscapes: true, ignoreClass: "tex2jax_ignore|dno" },
                                        TeX: {  noUndefined: { attributes: { mathcolor: "red", mathbackground: "#FFEEEE", mathsize: "90%" } }, Macros: { href: "{}" } },
                                        messageStyle: "none"
                });
                </script>
            <script src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full"></script>
            """


class RankingHtmlComposer(object):
    def __init__(self, question_id, data_object_source):
        self.question_id = question_id
        self.data_object_source = data_object_source
        try:
            self.question = self.data_object_source.get_question(self.question_id)
        except LookupError:
            self.question = None
        self.answer_set = self.data_object_source.get_answer_set(self.question_id)
        self.info_source = data_object_source.get_corresponding_info_source()


    def question_not_found_html(self):
        return "404 question with id %s not found!" % self.question_id

    def compose_predicted_ranking_html(self, trained_model, feature_source):
        self.answer_set.predict_scores(trained_model, feature_source)
        return self.compose_ranking_html()

    def compose_ranking_html(self):
        if self.question is None:
            return self.question_not_found_html()
        else:
            html = "<html>"
            html += self.compose_html_header()
            html += self.compose_html_body()
            html += "</html>"
            return html

    def compose_html_header(self):
        title = "Question %s" % self.question_id
        html = "<header>"
        html += "<title>%s</title>" % title
        html += mathjax_header
        html += self.compose_style_header()
        html += "</header"
        return html

    def compose_style_header(self):
        html = "<style>"
        with open(os.path.join(__location__, 'style.css'), 'r') as f:
            html += f.read()
        html += "</style>"
        return html

    def compose_html_body(self):
        html = "<body>"
        html += self.compose_title()
        html += self.compose_menu()
        html += self.compose_content()
        html += "</body>"
        return html

    def compose_title(self):
        html = "<h1>Showing ranking for question %s</h1>" % self.question_id
        return html

    def compose_menu(self):
        html = "<div id='menu'>"
        html += self.compose_menu_navigation()
        html += self.compose_infobox()
        html += "</div>"
        return html

    def compose_menu_navigation(self):
        question_ids = self.info_source.get_question_ids_of_questions_with_answers()
        i = question_ids.index(int(self.question_id))
        earlier_question_id = question_ids[(i-1)%len(question_ids)]
        next_question_id = question_ids[(i+1)%len(question_ids)]
        html = "<ul>"
        html += "<li><a href='/?question_id=%s'>Earlier Question &#8592;</a></li>" % earlier_question_id
        html += "<li><a href='/?question_id=%s'>Next Question &#8594;</a></li>" % next_question_id
        html += "</ul>"
        return html

    def compose_infobox(self):
        total_answers_displayed = len(self.answer_set)
        rmse_of_predictions = self.answer_set.get_rmse()
        kendall = self.answer_set.get_kendalls_tau("predicted_y", "ground_truths")
        html = "<div id='infobox'>"
        html += "Total Answers displayed: %s, " % total_answers_displayed
        html += "RMSE of predictions: %s, " % none_or_float2str(rmse_of_predictions)
        html += "Kendall's Tau (rel. to ground truth): %1.2f" % kendall
        html += "</div>"
        return html

    def compose_content(self):
        html = "<div id='content'>"
        html += self.compose_question_body()
        html += self.compose_answer_list()
        html += "</div>"
        return html

    def compose_question_body(self):
        html = "<div id='question'>"
        html += self.question.body
        html += "</div>"
        return html

    def compose_answer_list(self):
        html = "<ol>"
        our_ranks = self.answer_set.rank_by("predicted_y")
        for i, answer in enumerate(self.answer_set):
            score = answer.score
            predicted_y = answer.predicted_y
            predicted_score = answer.get_predicted_score()
            true_rank = our_ranks[i]
            html += "<li>"
            html += "vs %s.  <b>Score: %s, Prediction: %s (=%s)</b><br />" % (true_rank, score, predicted_y, predicted_score)
            html += answer.body
            html += "</li>"
        html += "</ol>"
        return html
