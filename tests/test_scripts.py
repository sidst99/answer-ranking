import unittest
import time
import os
import subprocess

from utils.sqlite import DatabaseConnection
from utils.settings import get_db, get_settings
from scripts.extenddb import *

test_db = get_db("beer")
test_dir = get_db("beer", "dir")


settings = get_settings("beer")
test_features_db = get_db("beer", "halfofagichteinfeatures")
test_doc2vec_model = get_db("beer", "doc2vec")
test_unigram_model = get_db("beer", "unigram")
test_report_pdf = os.path.join(test_dir, "*.pdf")
test_report_json = os.path.join(test_dir, "*.json")


class TestSQL(unittest.TestCase):
    def setUp(self):
        self.db = DatabaseConnection(test_db)
        self.clean_db()
        self.t0= float(time.time())

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)
        self.clean_db()

    def clean_db(self):
        remove_extensions(self.db)

    def test_knownquestions_table(self):
        defined_ratio = 0.5
        self.db.execute(knownquestions_table(defined_ratio))
        trainquestion_count = self.db.execute("SELECT count(*) FROM knownquestions").fetchone()
        total_posts_count = self.db.execute("SELECT count(*) FROM posts WHERE PostTypeId == 1").fetchone()
        ratio = float(trainquestion_count[0])/total_posts_count[0]
        print "Train ratio is %s" % ratio
        self.assertTrue(defined_ratio-0.2 < ratio < defined_ratio+0.2)

    #def test_testquestions_table(self):
    #    defined_ratio = 0.5
    #    self.db.execute(knownquestions_table(defined_ratio))
    #    self.db.execute(testquestions_table())
    #    trainquestion_count = self.db.execute("SELECT count(*) FROM knownquestions").fetchone()[0]
    #    testquestions_count = self.db.execute("SELECT count(*) FROM testquestions").fetchone()[0]
    #    total_posts_count = self.db.execute("SELECT count(*) FROM posts WHERE PostTypeId == 1").fetchone()[0]
    #    self.assertEqual(trainquestion_count + testquestions_count, total_posts_count)

    def test_total_votes_view(self):
        self.db.execute(posts_total_votes_view())
        result = tuple(self.db.execute("SELECT TotalVotes FROM totalvotes WHERE PostId == 1").fetchone())
        expected = (12,)
        self.assertEqual(result, expected)

    def test_total_comments_view(self):
        self.db.execute(posts_totalcomments_view())
        result = tuple(self.db.execute("SELECT * FROM totalcomments").fetchone())
        expected = (1, 3, 1.6666666666666667)
        self.assertEqual(result, expected)

    def test_extended_posts_table(self):
        self.db.execute(posts_total_votes_view())
        self.db.execute(posts_totalcomments_view())
        self.db.execute(extended_posts_table())
        result = tuple(self.db.execute("SELECT TotalReports FROM extendedposts WHERE PostId == 1").fetchone())
        expected = (0,)
        self.assertEqual(result, expected)
        result2 = tuple(self.db.execute("SELECT Score FROM extendedposts WHERE PostId == 2").fetchone())
        expected2 =  (21,)
        self.assertEqual(result2, expected2)

        c1 = self.db.execute("SELECT count(*) FROM posts")
        c2 = self.db.execute("SELECT count(*) FROM extendedposts")
        self.assertEqual(c1, c2)

    def test_answerers_view(self):
        self.db.execute(knownquestions_table(0.5))
        self.db.execute(posts_total_votes_view())
        self.db.execute(posts_totalcomments_view())
        self.db.execute(extended_posts_table())
        self.db.execute(answerers_view("knownquestions"))
        self.db.execute("SELECT * FROM answerers WHERE UserId == 8").fetchone()

    def test_question_view(self):
        self.db.execute(posts_total_votes_view())
        self.db.execute(posts_totalcomments_view())
        self.db.execute(extended_posts_table())
        self.db.execute(question_view())
        result = tuple(self.db.execute("SELECT AnswersAverageScore FROM questions WHERE QuestionId == 1").fetchone())
        expected = (8.333333333333334,)
        self.assertEqual(result, expected)

    def test_askers_view(self):
        self.db.execute(knownquestions_table(0.5))
        self.db.execute(posts_total_votes_view())
        self.db.execute(posts_totalcomments_view())
        self.db.execute(extended_posts_table())
        self.db.execute(question_view())
        self.db.execute(askers_view("knownquestions"))

    def test_extended_answers_table(self):
        extend_db(self.db, known_fraction=0.5)
        self.db.execute("SELECT * FROM extendedanswers WHERE PostId == 6").fetchone()
        nb_extendedanswers = self.db.execute("SELECT count(*) FROM extendedanswers").fetchone()[0]
        nb_answers = self.db.execute("SELECT count(*) FROM posts WHERE PostTypeId == 2").fetchone()[0]
        ratio = float(nb_extendedanswers)/nb_answers
        self.assertTrue(0.3 < ratio < 0.7, msg="(nb_extendedanswers)/nb_answers = %s" % ratio)


class TestMake(unittest.TestCase):
    def setUp(self):
        self.t0= float(time.time())

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)

    def partly_clean_test_dir(self):
        command = "find %s -name '*.pdf' -o -name '*.json' -o -name '*.log' -type f -exec rm -f {} +" % test_dir
        os.system(command)

    def clean_test_dir(self):
        command = "find %s ! -name '*.xml' -type f -exec rm -f {} +" % test_dir
        os.system(command)

    def test_make_sh_on_clean_system(self):
        self.clean_test_dir()
        print "Running make.sh..."
        os.system("../scripts/make.sh beer")
        self.assertTrue(os.path.isfile(test_db))
        self.assertTrue(os.path.isfile(test_features_db))
        self.assertTrue(os.path.isfile(test_doc2vec_model))
        self.assertTrue(os.path.isfile(test_unigram_model))
        self.assertTrue(self.files_created(test_report_pdf))
        self.assertTrue(self.files_created(test_report_json))

    def test_make_sh_on_full_system(self):
        t0= float(time.time())
        os.system("../scripts/make.sh beer")
        t = float(time.time()) - t0
        print "Took %s to run make.sh with already existing files" % t
        self.assertTrue(t < 6)

    def test_run_makedb(self):
        os.system("../scripts/makedb.py beer")

    def test_run_makedoc2vec(self):
        os.system("../scripts/makedoc2vec.py beer")

    def test_run_makefeatures(self):
        os.system("../scripts/makefeatures.py beer")

    def test_run_makereports(self):
        os.system("../scripts/makereport.py beer")

    def test_run_makequestioning(self):
        # runs on empty (since there are no doc2vecmodel_* files around in fixture by default
        os.system("../scripts/makequestioning.py beer")

    def files_created(self, filepath):
        c = "ls %s" % filepath
        try:
            subprocess.check_output(c, shell=True)
        except subprocess.CalledProcessError:
            return False
        return True

    def remove_files(self, filepath):
        c = "rm %s" % filepath
        try:
            os.system(c)
        except OSError:
            pass

