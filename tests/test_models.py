import unittest
import time

from models.evaluatedoc2vec import Dov2VecModelEvaluator, QuestionGenerator
from models.featuresource import *
from models.errorfunctions import *
from models.model import *
from models.evaluate import *
from stackexchangedata.db import InfoSource
from scripts.extenddb import extend_db, remove_extensions
from utils.settings import get_db
from stackexchangedata.db import DataObjectSource

test_db = get_db("beer")
test_dir = get_db("beer", "dir")
features_db = get_db("beer", "halfofagichteinfeatures")


from gensim.models import Doc2Vec
doc2vec_model = Doc2Vec.load(os.path.join(test_dir, "doc2vecmodel"))


def setUpModule():
    db = DatabaseConnection(test_db)
    remove_extensions(db)
    extend_db(db)


class TestFeaturesource(unittest.TestCase):
    def setUp(self):
        self.s = FeatureSource(features_db)
        self.dimensionality = 8
        # N = number of data samples have features for
        self.samples = self.s.execute("SELECT count(*) FROM features").fetchone()[0]
        self.test_question = self.s.get_question_ids()[0]
        self.test_answer = self.s.get_answer_ids()[0]
        self.test_answers = self.s.get_answer_ids()[:3]

    def test_get_train_and_test_questions(self):
        tr = self.s.train_questions
        te = self.s.test_questions
        self.assertEqual(type(tr), list)
        self.assertEqual(type(te), list)
        nb_questions = self.s.execute("SELECT count(*) FROM (SELECT DISTINCT ParentId FROM features)").fetchone()[0]
        self.assertEqual(len(tr) + len(te), nb_questions)

    def test_get_X(self):
        self.assertEqual(self.s.get_X().shape, (self.samples, self.dimensionality))

    def test_get_y(self):
        self.assertEqual(self.s.get_y().shape, (self.samples, 1))

    def test_get_features(self):
        self.assertEqual(self.s.get_features(self.test_answer).shape, (1, self.dimensionality))

    def test_get_ground_truth(self):
        self.assertTrue(-1 <= self.s.get_ground_truth(self.test_answer) <= 1)

    def test_init_train_test_split(self):
        # make sure features and ground truths actually match
        features = self.s.X_train.tolist()
        truth = self.s.y_train.tolist()
        for i, _ in enumerate(features):
            f = features[i]
            t = truth[i][0]
            query = "SELECT Features FROM features WHERE Truth == %s" % t
            r = self.s.execute(query).fetchall()
            r = [eval(e[0]) for e in r]
            # since the mapping of truth -> feature is not 1:1 we just check
            # if there is at least one matching feature vector
            # the probability that this fails should be extremely small
            self.assertIn(tuple(f), r)

    def test_X_test_and_y_test(self):
        number_of_samples_X_test = self.s.X_test.shape[0]
        dimensionality_X_test = self.s.X_test.shape[1]
        self.assertEqual(dimensionality_X_test, self.dimensionality)
        self.assertTrue(1 <= number_of_samples_X_test <= 61)

        number_of_samples_y_test = self.s.y_test.shape[0]
        self.assertEqual(number_of_samples_X_test, number_of_samples_y_test)

    def test_get_multiple_features(self):
        features = self.s.get_multiple_features(self.test_answers)
        self.assertEqual(type(features), list)
        self.assertEqual(len(features), len(self.test_answers))
        self.assertEqual(len(features[0]), self.dimensionality)

    def test_get_multiple_truths(self):
        truths = self.s.get_multiple_truths(self.test_answers)
        self.assertEqual(len(truths), len(self.test_answers))
        for t in truths:
            self.assertTrue(-1 <= t <= 1)

    def test_get_features_by_parent_id(self):
        features = self.s.get_features_by_parent_id(self.test_question)
        self.assertGreater(features.shape[0], 0)
        self.assertEqual(features.shape[1], self.dimensionality)

    def test_get_truths_by_parent_id(self):
        truths = self.s.get_truths_by_parent_id(self.test_question)
        self.assertIsInstance(truths, numpy.ndarray)
        self.assertEqual(truths.shape[1], 1)

    def test_feature_type(self):
        expected = "half-agichtein"
        actual = self.s.type
        self.assertEqual(expected, actual)

    def test_get_answer_ids(self):
        self.assertEqual(type(self.s.get_question_ids()), list)
        # there is maximally one question per answer, or minimally 1 question that holds all answers:
        self.assertTrue(1 <= len(self.s.get_question_ids()) <= self.samples)

    def test_get_reporting_question_ids(self):
        tr, te = self.s.get_reporting_question_ids(3)
        self.assertTrue(set(tr).issubset(set(self.s.train_questions)))
        self.assertTrue(set(te).issubset(set(self.s.test_questions)))


class TestModel(unittest.TestCase):
    def setUp(self):
        self.models = [Model(model_name) for model_name in models.keys()]
        self.feature_source = FeatureSource(features_db)
        self.cross_validating_models = cv_models

    def test_model_methods(self):
        for m in self.models:
            self.assertTrue(callable(m.fit))
            self.assertTrue(callable(m.predict))

    def test_feature_importances(self):
        for m in self.models:
            self.assertTrue(m.feature_importances() is not None, msg="%s has feature_importances() == None" % m.name)

    def test_train(self):
        for m in self.models:
            print m.name

    def test_get_model_name(self):
        for m in self.models:
            self.assertIsInstance(m.name, str)

    def test_best_params(self):
        for m in self.models:
            m.train(self.feature_source)
            if m.name in self.cross_validating_models:
                self.assertTrue(m.best_params() is not None, msg="%s has best_params() == None" % m.name)

    def test_set_cv_param_grid(self):
        for m in self.models:
            if m.name in self.cross_validating_models:
                m.set_cv_param_grid({})
            else:
                self.assertRaises(AttributeError, m.set_cv_param_grid, {})

    def test_make_fancy(self):
        for m in self.models:
            m.make_fancy()


class TestErrorfuncitons(unittest.TestCase):
    def test_mse(self):
        v1 = numpy.array([1,2])
        v2 = numpy.array([2,3])
        self.assertEqual(mse(v1, v2), 1)

    def test_rmse(self):
        v1 = numpy.array([1,2])
        v2 = numpy.array([4,5])
        self.assertEqual(rmse(v1, v2), 3)

    def test_mse_of_residuals(self):
        v1 = numpy.array([2,2])
        self.assertEqual(mse(v1), 4)

    def test_mse_of_list_not_array(self):
        v1 = [1,2]
        v2 = [2,3]
        self.assertEqual(mse(v1, v2), 1)

    def test_rmse_list_of_none(self):
        v1 = [None, None]
        self.assertEqual(rmse(v1), None)

    def test_nb_discordant_pairs(self):
        l = [1, 2, 3, 4]
        self.assertEqual(excess_concordant_pairs(l), 6)
        l = [2, 1, 3, 4]
        self.assertEqual(excess_concordant_pairs(l), 4)
        l = [2, 1, 4, 3]
        self.assertEqual(excess_concordant_pairs(l), 2)
        l = [3, 2, 1, 4]
        self.assertEqual(excess_concordant_pairs(l), 0)
        l = [4, 3, 2, 1]
        self.assertEqual(excess_concordant_pairs(l), -6)

    def test_sign(self):
        self.assertEqual(sign(-3), -1)
        self.assertEqual(sign(0), 1)
        self.assertEqual(sign(2), 1)
        self.assertEqual(sign(-0.2), -1)

    def test_kendalls_tau(self):
        l = [1, 2, 3, 4]
        self.assertEqual(kendalls_tau(l), 1)
        l = [3, 2, 1, 4]
        self.assertEqual(kendalls_tau(l), 0)
        l = [4, 3, 2, 1]
        self.assertEqual(kendalls_tau(l), -1)
        l = [1]
        self.assertEqual(kendalls_tau(l), 1)
        l = []
        self.assertEqual(kendalls_tau(l), 1)

    def test_random_kendall_distribution(self):
        weighted_lengths = {2: 1.0}
        self.assertEqual(random_kendall_distribution(weighted_lengths), [1, -1])
        weighted_lengths = {2: 0.5, 3: 0.5}
        self.assertEqual(random_kendall_distribution(weighted_lengths), [1.0, 1.0, 1.0, -0.3333333333333333, 0.3333333333333333, -1.0, -1.0, -1.0])
        weighted_lengths = {2: 0.5, 3: 0.4}
        self.assertRaises(ValueError, random_kendall_distribution, weighted_lengths)
        weighted_lengths = {}
        self.assertEqual(random_kendall_distribution(weighted_lengths), [])


class TestModelEvaluator(unittest.TestCase):
    def setUp(self):
        self.feature_source = FeatureSource(features_db)
        model = Model("linear_regression", self.feature_source)
        info_source = InfoSource(test_db)
        self.evaluator = ModelEvaluator(model, info_source, self.feature_source)
        self.question_ids = self.feature_source.get_question_ids_with_min_answers(3)
        self.samples = self.feature_source.execute("SELECT count(*) FROM features").fetchone()[0]

    def test_get_rmse_for_question(self):
        rmse = self.evaluator.get_rmse(self.question_ids[0])
        self.assertTrue(0 < rmse < 2, msg="RMSE was %s" % rmse)

    def test_get_train_rmse(self):
        rmse = self.evaluator.get_train_rmse()
        self.assertTrue(0 < rmse < 1, msg="RMSE was %s" % rmse)

    def test_get_test_rmse(self):
        test_rmse = self.evaluator.get_test_rmse()
        train_rmse = self.evaluator.get_train_rmse()
        self.assertTrue(0 < test_rmse < 2, msg="Test RMSE was %s" % test_rmse)
        self.assertTrue(test_rmse > train_rmse, msg="Test RMSE: %s, Train RMSE: %s" % (test_rmse, train_rmse))

    def test_get_residuals(self):
        res = self.evaluator.get_residuals()
        self.assertEqual(res.shape, (self.samples, 1))

    def test_compose_report_path(self):
        dir = os.path.dirname(test_db)
        name = "report_fixtures_linear_regression_half-agichtein"
        expected_path = os.path.join(dir, name)
        actual_path = self.evaluator.compose_report_path()
        self.assertEqual(actual_path, expected_path)

    def test_train_and_test_report_paths(self):
        report_names = [
            'report_fixtures_linear_regression_half-agichtein_train.pdf',
            'report_fixtures_linear_regression_half-agichtein_test.pdf',
            'report_fixtures_linear_regression_half-agichtein_train.json',
            'report_fixtures_linear_regression_half-agichtein_test.json'
        ]
        expected_files = [os.path.join(test_dir, n) for n in report_names]
        self.assertEqual(self.evaluator.list_train_and_test_files(), expected_files)

    def test_calculate_report_content(self):
        question_ids = self.feature_source.get_question_ids_with_min_answers(3)
        content = self.evaluator.calculate_report_content(question_ids)
        self.assertTrue(len(content["tau_score"]) <= 9) # the complete beer set contains 9
        self.assertTrue(-1 <= content["tau_score"][0] <= 1)
        self.assertTrue(len(content["rmse"]) <= 9)
        self.assertTrue(0 < content["rmse"][0])
        self.assertTrue(len(content["tau_y"]) <= 9)
        self.assertTrue(-1 <= content["tau_y"][0] <= 1)

    def test_create_report(self):
        self.evaluator.create_report(self.question_ids, show_plots=False)
        expected_pdf_path = self.evaluator.compose_report_path()+".pdf"
        expected_json_path = self.evaluator.compose_report_path()+".json"
        self.assertTrue(os.path.isfile(expected_pdf_path))
        self.assertTrue(os.path.isfile(expected_json_path))
        os.remove(expected_pdf_path)
        os.remove(expected_json_path)


class TestReportContent(unittest.TestCase):
    def setUp(self):
        self.rc = ReportContent()
        self.data = DataObjectSource(test_db)
        self.features = FeatureSource(features_db)
        self.model = Model("linear_regression", self.features)
        self.question_ids = self.features.get_question_ids()

        self.full_rc = ReportContent()
        self.answer_sets = [self.data.get_answer_set(i) for i in self.question_ids]
        for answer_set in self.answer_sets:
            answer_set.predict_scores(self.model, self.features)
        self.full_rc.add_answer_sets(self.answer_sets)

    def test_add_answer_set_metrics(self):
        question_id = self.question_ids[0]
        answer_set = self.data.get_answer_set(question_id)
        answer_set.predict_scores(self.model, self.features)
        self.rc.add_answer_set_metrics(answer_set)
        for k, v in self.rc.report_content.iteritems():
            self.assertTrue(v == None or len(v) == 1)

    def test_add_answer_set(self):
        for k, v in self.full_rc.report_content.iteritems():
            self.assertTrue(v == None or len(v) == len(self.answer_sets))

    def test_add_model_info(self):
        self.rc.add_model_info(self.model)
        coefs = self.rc["feature_importances"]
        self.assertTrue(type(coefs) is list)

    def test_add_overview(self):
        self.rc.add_overview()
        self.assertTrue(type(self.rc["overview"]) is list)


class TestDoc2VecEvaluator(unittest.TestCase):
    def setUp(self):
        self.min_answers = 4
        self.question_generator = QuestionGenerator(InfoSource(test_db))
        self.question_generator.show_progress = False
        self.question_generator.compose_cross_questions(min_answers=self.min_answers)
        self.evaluator = Dov2VecModelEvaluator(doc2vec_model)
        self.evaluator.show_progress = False
        self.t0= float(time.time())
        self.featureset = None

    def tearDown(self):
        t = float(time.time()) - self.t0
        print "%s: %.3f" % (self.id(), t)

    def test_compute_model_accuracy(self):
        questions = self.question_generator.get_questions()
        expected_incorrect = [
            'QUESTION_8 ANSWER_23 QUESTION_22 ANSWER_25',
            'QUESTION_8 ANSWER_23 QUESTION_47 ANSWER_49',
            'QUESTION_22 ANSWER_25 QUESTION_8 ANSWER_23',
            'QUESTION_22 ANSWER_25 QUESTION_47 ANSWER_49',
            'QUESTION_47 ANSWER_49 QUESTION_8 ANSWER_23',
            'QUESTION_47 ANSWER_49 QUESTION_22 ANSWER_25'
        ]
        expected = {"correct": [], "incorrect": expected_incorrect}
        real = self.evaluator.compute_model_accuracy(questions)
        self.assertEqual(expected, real)

    def test_compute_model_accuracy_with_answer_choices(self):
        questions = self.question_generator.get_questions()
        answer_choices = self.question_generator.answer_choices
        real = self.evaluator.compute_model_accuracy(questions, answer_choices)
        expected = {'incorrect': [
            'QUESTION_8 ANSWER_23 QUESTION_22 ANSWER_25',
            'QUESTION_8 ANSWER_23 QUESTION_47 ANSWER_49',
            'QUESTION_22 ANSWER_25 QUESTION_8 ANSWER_23',
            'QUESTION_47 ANSWER_49 QUESTION_8 ANSWER_23',
            'QUESTION_47 ANSWER_49 QUESTION_22 ANSWER_25'],
        'correct': [
            'QUESTION_22 ANSWER_25 QUESTION_47 ANSWER_49']}
        self.assertEqual(real, expected)

    def test_most_similar_among_docs(self):
        docs = ["QUESTION_1", "ANSWER_2", "QUESTION_3"]
        most_similar = self.evaluator.most_similar_among_docs(docs, positive="QUESTION")
        self.assertIsInstance(most_similar, list)
        self.assertEqual(len(most_similar), len(docs))
        self.assertTrue(most_similar[0][1] in docs)

    def test_model_answers_correctly(self):
        q = self.question_generator.get_questions()[0]
        self.evaluator.model_answers_correctly(q)
        p = self.question_generator.answer_choices[q.split()[2]]
        self.evaluator.model_answers_correctly(q, possible_answers=p)

    def test_mean_and_std_of_question_answer_similarities(self):
        answer_choices = self.question_generator.answer_choices
        m, s = self.evaluator.mean_and_std_of_question_answer_similarities(answer_choices)
        self.assertEqual(type(m), list)
        self.assertEqual(type(s), list)
        self.assertEqual(len(m), len(s))

    def test_random_guess_percent_right_answer(self):
        answer_choices = self.question_generator.answer_choices
        truth = self.evaluator.random_guess_percent_right_answer(answer_choices)
        self.assertTrue(truth, 1.0/self.min_answers)


class TestQuestionGenerator(unittest.TestCase):
    def setUp(self):
        self.info_source = InfoSource(test_db)
        self.qg = QuestionGenerator(self.info_source)
        self.qg.show_progress = True

    def test_compose_cross_questions(self):
        expected = [
            'QUESTION_8 ANSWER_23 QUESTION_22 ANSWER_25',
            'QUESTION_8 ANSWER_23 QUESTION_47 ANSWER_49',
            'QUESTION_22 ANSWER_25 QUESTION_8 ANSWER_23',
            'QUESTION_22 ANSWER_25 QUESTION_47 ANSWER_49',
            'QUESTION_47 ANSWER_49 QUESTION_8 ANSWER_23',
            'QUESTION_47 ANSWER_49 QUESTION_22 ANSWER_25'
        ]
        self.qg.compose_cross_questions(min_answers=4)
        self.assertEqual(self.qg.generated_questions, expected)

    def test_compose_local_cross_questions(self):
        expected = [
            'QUESTION_1 ANSWER_2 QUESTION_47 ANSWER_49',
            'QUESTION_16 ANSWER_17 QUESTION_22 ANSWER_25',
            'QUESTION_16 ANSWER_17 QUESTION_1 ANSWER_2',
            'QUESTION_22 ANSWER_25 QUESTION_16 ANSWER_17',
            'QUESTION_47 ANSWER_49 QUESTION_1 ANSWER_2'
        ]
        self.qg.compose_local_cross_questions(doc2vec_model, min_answers=3)
        self.assertEqual(self.qg.generated_questions, expected)


    def test_compose_general_questions(self):
        expected = [
            'QUESTION ANSWER QUESTION_8 ANSWER_23',
            'QUESTION ANSWER QUESTION_22 ANSWER_25',
            'QUESTION ANSWER QUESTION_47 ANSWER_49'
        ]
        self.qg.compose_general_questions(min_answers=4)
        self.assertEqual(self.qg.generated_questions, expected)

    def test_answer_choices(self):
        self.qg.compose_question_answer_pairs(min_answers=4)
        self.assertEqual(set(self.qg.answer_choices.keys()), set(['QUESTION_8', 'QUESTION_22', 'QUESTION_47']))


class TestOverviewer(unittest.TestCase):
    def setUp(self):
        self.o = Overviewer(test_dir)

    def test_reportname(self):
        expected = "linear_regression_half-agichtein"
        real = self.o.report_name("/Users/dmeier/Dropbox/epfl-masterthesis/masterproject/fixtures/report_fixtures_linear_regression_half-agichtein_test.json")
        self.assertEqual(real, expected)

    def test_data_group_name(self):
        expected = "beer"
        real = self.o.data_group_name("/Users/dmeier/Dropbox/epfl-masterthesis/masterproject/fixtures/")
        self.assertEqual(real, expected)

    def test_load_reports(self):
        self.assertEqual(type(self.o.reports), dict)
        self.assertGreater(len(self.o.reports), 0)
        for d, reports_in_d in self.o.reports.items():
            self.assertEqual(type(reports_in_d), dict)
            self.assertGreater(len(reports_in_d), 0)
            for report, content in reports_in_d.items():
                self.assertEqual(type(content["rmse"]), list)
                self.assertEqual(type(content["tau_y"]), list)

    def test_compose_multibar_groups(self):
        for group, tags in self.o.compose_multibar_groups("rmse").items():
            self.assertIsInstance(tags, collections.OrderedDict)
            for tag, value in tags.items():
                self.assertEqual(type(value), tuple)

    def test_compose_boxplot_series(self):
        for label, series in self.o.compose_boxplot_series("rmse").items():
            self.assertEqual(type(series), list)

    def test_compose_figures(self):
        figures = self.o.compose_figures()
        for f in figures:
            self.assertIsInstance(f, plt.Figure)