import unittest

from stackexchangedata.reporttoolbox import *
from stackexchangedata.db import *
from stackexchangedata.objects import *

from models.model import Model
from models.featuresource import FeatureSource

from utils.settings import get_db
from scripts.extenddb import remove_extensions, extend_db

test_db = get_db("beer")
test_features_db = get_db("beer", "halfofagichteinfeatures")


def setUpModule():
    db = DatabaseConnection(test_db)
    remove_extensions(db)
    extend_db(db)


def get_model_and_feature_source():
    feature_source = FeatureSource(test_features_db)
    model = Model("linear_regression", feature_source)
    return model, feature_source


class TestInfoSource(unittest.TestCase):
    def setUp(self):
        self.source = InfoSource(test_db)

    def test_get_question_answer_timings(self):
        answer = self.source.get_question_answer_timings()
        expected_answer = [u'2014-01-21T20:58:43.500', u'2014-01-21T21:05:11.577',
                           u'2014-01-22T04:46:53.373', u'2014-01-23T08:44:14.440']
        self.assertEqual(answer[1], expected_answer)

    def test_get_question_creation_dates(self):
        answer = self.source.get_question_creation_date_tuples()
        self.assertEqual(tuple(answer[0]), (1, "2014-01-21T20:58:43.500"))

    def test_get_answer_timings(self):
        answer = self.source.get_answer_timings(answer_number=1)
        self.assertEqual(answer[0], 461.6966)
        self.assertIsInstance(answer, list)

    def test_get_question_ids(self):
        self.assertEqual(self.source.get_question_ids(),
                         [1, 3, 4, 5, 8, 10, 11, 16, 22,
                          24, 27, 29, 33, 42, 47, 51, 53,
                          55, 59, 64, 67, 69, 70, 74, 77,
                          79, 82, 84, 88, 89, 92, 94, 98,
                          100, 103, 104, 105, 108])

    def test_get_question_ids_of_questions_with_answers(self):
        question_ids = self.source.get_question_ids_of_questions_with_answers()
        self.assertEqual(question_ids,
                         [1, 3, 4, 5, 8, 10, 11, 16, 22, 24,
                          27, 29, 33, 42, 47, 51, 53, 55, 59,
                          64, 67, 70, 74, 77, 79, 82, 88, 89,
                          92, 94, 98, 100, 104, 108])
        self.assertEqual(question_ids.index(3), 1)

    def test_get_ids_of_questions_with_n_answers(self):
        question_ids = self.source.get_ids_of_questions_with_at_least_n_answers(3)
        self.assertEqual(question_ids, [1, 8, 11, 16, 22, 29, 33, 47, 70])
        question_ids2 = self.source.get_ids_of_questions_with_at_least_n_answers(4)
        self.assertEqual(question_ids2, [8, 22, 47])

    def test_get_comments_per_answer(self):
        counts = self.source.get_comments_per_answer()
        self.assertEqual(counts, [0, 0, 0, 0, 1, 4, 3, 15, 1, 0, 0, 0, 0, 0, 2, 0, 2, 1, 0, 0,
                                  0, 0, 4, 1, 2, 0, 0, 2, 0, 1, 3, 11, 2, 0, 0, 7, 0, 1, 2, 0,
                                  0, 1, 0, 0, 1, 0, 1, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

    def test_get_viewcount_vs_total_votes(self):
        vc, tv = self.source.get_viewcount_vs_total_votes()
        self.assertEqual(len(vc), len(tv))
        self.assertEqual(type(vc), list)
        self.assertEqual(type(tv), list)


class TestDocumentSource(unittest.TestCase):
    def test_iter(self):
        from gensim.models.doc2vec import TaggedDocument
        l = list(DocumentSource(test_db))
        self.assertEqual(len(l), 208)
        self.assertIsInstance(l[0], TaggedDocument)
        self.assertEqual(l[5].tags[0], 'QUESTION_3')
        self.assertEqual(l[2].tags, ['ANSWER_2', 'ANSWER'])
        self.assertEqual(l[18].tags, ['QUESTION_10', 'QUESTION', 'QUESTION_11', 'discussion', 'scope'])
        words15 = [u'seems', u'site', u'going', u'succeed', u"'re", u'going',
                  u'get', u'nitty', u'gritty', u'.', u'questions', u"'ve", u'seen',
                  u'far', u'asking', u'general', u'case', u'``', u'beer', u'.', u"''",
                  u'seems', u'poor', u'way', u'attracting', u'experts', u'topic', u'.']
        document15 = TaggedDocument(words=words15, tags=['QUESTION_8', 'QUESTION', u'discussion'])
        self.assertEqual(l[15], document15)

    def test_iter_no_split(self):
        l = list(DocumentSource(test_db, split=False))
        self.assertEqual(len(l), 208)
        self.assertEqual(type(l[5]), tuple)
        self.assertEqual(len(l[5]), 403)
        for c in l[5]:
            self.assertEqual(len(c), 1)

    def test_iter_constrain_by_knownquestions(self):
        l = list(DocumentSource(test_db, constrain_by='knownquestions'))
        self.assertLess(len(l), len(list(DocumentSource(test_db))))

    def test_iter_constrain_by_int(self):
        constrain_by = 10
        l = list(DocumentSource(test_db, constrain_by=constrain_by))
        self.assertLess(len(l), 2*constrain_by+1)


class TestReportToolbox(unittest.TestCase):
    def setUp(self):
        source = InfoSource(test_db)
        self.toolbox = ReportToolbox(source)

    def test_can_be_initiated(self):
        self.assertIsInstance(self.toolbox, ReportToolbox)


class TestDataObjectSource(unittest.TestCase):
    def setUp(self):
        self.source = DataObjectSource(test_db)

    def test_get_commands(self):
        self.assertIsInstance(self.source.get_answer(2), Answer)
        self.assertIsInstance(self.source.get_answer_set(1), AnswerSet)
        self.assertIsInstance(self.source.get_answers_given_to_user(1), AnswerSet)
        self.assertIsInstance(self.source.get_question(1), Question)
        self.assertIsInstance(self.source.get_questions_asked_by_user(1), QuestionSet)
        self.assertIsInstance(self.source.get_corresponding_info_source(), InfoSource)

    def test_get_answer_sets(self):
        answer_sets = self.source.get_answer_sets([1, 3])
        self.assertIsInstance(answer_sets, list)
        self.assertIsInstance(answer_sets[0], AnswerSet)
        self.assertEqual(len(answer_sets[0]), 3)


class TestAnswer(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)
        self.answer = self.data.get_answer(2)

    def test_init_answer_features(self):
        self.assertEqual(self.answer.id, 2)

    def test_score(self):
        self.assertEqual(self.answer.score, 21)

    def test_total_votes(self):
        self.assertEqual(self.answer.total_votes, 21)

    def test_answer_is_a_question(self):
        self.assertRaises(LookupError, self.data.get_answer, 1)

    def test_predict_score(self):
        model, feature_source = get_model_and_feature_source()
        try:
            predicted_y = self.answer.predict_y(model, feature_source)
            self.assertTrue(-1.2 < predicted_y < 1.2, msg="predicted_y is %s" % predicted_y)
        except LookupError: # happens if, by chance, Answer(2) is in the known answers
            pass
        predicted_score = self.answer.get_predicted_score()
        self.assertTrue(0 <= predicted_score <= 25 or predicted_score is None)

    def test_get_predicted_score(self):
        self.assertEqual(self.answer.get_predicted_score(), None)

    def test_get_arrival_number(self):
        answer1 = self.answer
        answer2 = self.data.get_answer(19)
        answer3 = self.data.get_answer(40)
        self.assertEqual(answer1.get_arrival_number(), 1)
        self.assertEqual(answer2.get_arrival_number(), 2)
        self.assertEqual(answer3.get_arrival_number(), 3)

    def test_get_ground_truth(self):
        self.assertAlmostEqual(self.answer.get_ground_truth(), 0.9545454545454546 )


class TestAnswerSet(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)
        self.answer_set = self.data.get_answer_set(1)

    def test_str(self):
        self.assertEqual(str(self.answer_set), "<Answer object: Id 2>, <Answer object: Id 40>, <Answer object: Id 19>")

    def test_get_scores(self):
        self.assertEqual(sum(self.answer_set.get("scores")), 25)

    def assertIsEmptyAnswerSet(self, answer_set):
        self.assertEqual(answer_set.elements, [])
        self.assertEqual(answer_set.get("scores"), [])
        self.assertEqual(answer_set.get("owner_reputations"), [])

    def test_empty_answer_set(self):
        # questions with no answers are 69, 84, 103 and 105
        answer_set = self.data.get_answer_set(69)
        self.assertIsEmptyAnswerSet(answer_set)

    def test_non_existent_answer_set(self):
        answer_set = self.data.get_answer_set(2)
        self.assertIsEmptyAnswerSet(answer_set)
        self.assertEqual(answer_set.parent_id, None)

    def test_get_ids(self):
        self.assertEqual(self.answer_set.get("ids"), [2, 40, 19])

    def test_get_ground_truths(self):
        self.assertEqual(self.answer_set.get("ground_truths"), [0.9545454545454546, 0.75, 0.5])

    def test_get_errors(self):
        self.assertEqual(self.answer_set.get("errors"), [None, None, None])

    def test_get_rmse_not_predicted(self):
        self.assertEqual(self.answer_set.get_rmse(), None)

    def test_get_rmse(self):
        model, feature_source = get_model_and_feature_source()
        try:
            self.answer_set.predict_scores(model, feature_source)
            rmse = self.answer_set.get_rmse()
            self.assertTrue(0 < rmse < 2, msg="RMSE was %s" % rmse)
        except LookupError:
            pass

    def test_kendalls_tau(self):
        tau = self.answer_set.get_kendalls_tau("predicted_y", "scores")
        self.assertTrue(-1 <= tau <= 1)
        tau2 = self.answer_set.get_kendalls_tau("predicted_y", "ground_truths")
        self.assertTrue(-1 <= tau2 <= 1)

    def test_parent_id(self):
        self.assertEqual(self.answer_set.parent_id, 1)

    def test_same_best_answer(self):
        original_sorted_by = self.answer_set.sorted_by
        b1 = self.answer_set.same_best_answer("scores", "ground_truths") # 2-40-19 vs 40-2-19
        self.assertEqual(b1, True)
        self.assertEqual(self.answer_set.sorted_by, original_sorted_by)
        b2 = self.answer_set.same_best_answer("predicted_y", "ground_truths") # 2-40-19 vs 2-40-19
        self.assertIsInstance(b2, bool)
        self.assertEqual(self.answer_set.sorted_by, original_sorted_by)

    def test_ndgc(self):
        original_sorted_by = self.answer_set.sorted_by
        ndcg = self.answer_set.ndcg()
        self.assertTrue(0 <= ndcg <= 1, msg="NDCG was %s not between 0 and 1" % ndcg)
        self.assertEqual(self.answer_set.sorted_by, original_sorted_by)

    def test_compute_mmr_ranks(self):
        from gensim.models import Doc2Vec
        doc2vec = get_db("beer", "doc2vec")
        doc2vec_model = Doc2Vec.load(doc2vec)
        model, feature_source = get_model_and_feature_source()
        # should raise exception if answer set doesn't have predicted scores yet
        self.assertRaises(ValueError, self.answer_set.compute_mmr_ranks, doc2vec_model.docvecs.n_similarity)
        try:
            self.answer_set.predict_scores(model, feature_source)
            self.answer_set.compute_mmr_ranks(doc2vec_model.docvecs.n_similarity)
            self.assertEqual(type(self.answer_set.get("mmr_ranks")), list)
        except LookupError:
            pass

    def test_set_clusters(self):
        clusters = [1,2,3]
        self.answer_set.set_clusters(clusters)
        self.assertEqual(self.answer_set[0].cluster, 1)
        self.assertEqual(self.answer_set[1].cluster, 2)
        self.assertEqual(self.answer_set[2].cluster, 3)
        self.assertRaises(ValueError, self.answer_set.set_clusters, [1,2])
        self.assertRaises(ValueError, self.answer_set.set_clusters, [1, 2, 3, 4])

    def test_evaluation_metric(self):
        clusters = [1,2,1]
        self.answer_set.set_clusters(clusters)
        metrics = self.answer_set.evaluation_metric("ground_truths", 0.5)
        self.assertEqual(metrics[0], 5.0)
        self.assertEqual(metrics[1], 2.2045454545454546)
        self.assertEqual(metrics[2], 4.113636363636363)


class TestUser(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)

    def test_properties_of_normal_user(self):
        self.user = self.data.get_user(8)
        self.assertEqual(self.user.age, 33)
        self.assertTrue(0 <= self.user.answers_accepted <= 2)
        self.assertTrue(0 <= self.user.answers_written <= 6)
        self.assertTrue(0 <= self.user.downvotes_received <= 1)

    def test_properties_of_user_with_missing_values(self):
        self.user = self.data.get_user(1)
        self.assertEqual(self.user.answers_accepted, 0)
        self.assertEqual(self.user.answers_written, 0)
        self.assertEqual(self.user.age, 39)
        self.assertEqual(self.user.downvotes_received, 0)

    def test_non_existent_user(self):
        self.assertRaises(LookupError, self.data.get_user, 9)

    def test_properties_of_default_unknown_user(self):
        default_user = self.data.get_user(None)
        self.assertEqual(default_user.reputation, 1)
        self.assertEqual(default_user.answers_accepted, 0)

    def test_get_corresponding_answers_received(self):
        self.user = self.data.get_user(8)
        answers_received = self.user.get_corresponding_answers_received()
        self.assertEqual(len(answers_received), 1)
        self.assertIsInstance(answers_received[0], Answer)
        self.default_user = self.data.get_user(None)
        self.assertEqual(self.default_user.get_corresponding_answers_received(), [])

    def test_get_corresponding_questions_asked(self):
        self.user = self.data.get_user(8)
        questions_asked = self.user.get_corresponding_questions_asked()
        self.assertEqual(len(questions_asked), 1)
        self.assertIsInstance(questions_asked[0], Question)


class TestComment(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)

    def test_properties_of_normal_comment(self):
        self.comment = self.data.get_comment(1)
        self.assertEqual(self.comment.id, 1)
        self.assertEqual(self.comment.post_id, 10)
        self.assertEqual(self.comment.score, 4)

    def test_non_existent_comment(self):
        self.assertRaises(LookupError, self.data.get_comment, 137)


class TestCommentSet(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)
        self.comment_set = self.data.get_comment_set(10)

    def test_get_scores(self):
        self.assertEqual(self.comment_set.get_scores(), [4, 0])

    def test_get_average_score(self):
        self.assertEqual(self.comment_set.get_average_score(), 2.0)

    def test_get_nb_comments(self):
        self.assertEqual(self.comment_set.get_nb_comments(), 2)


class TestExtendedAnswer(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)
        a = self.data.execute("SELECT PostId FROM extendedanswers").fetchone()[0]
        self.ea = self.data.get_extended_answer(a)

    def test_init(self):
        votes_received = self.ea.properties["AnswererVotesReceived"]
        # votes received can be a number or none (__getitem__ will convert None to 0 later for feature calculation)
        self.assertTrue(0 <= votes_received <= 54 or votes_received is None, msg="Votes received is %s" % votes_received)


class TestQuestion(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)

    def test_properties_of_a_normal_question(self):
        self.q = self.data.get_question(1)
        self.assertEqual(self.q.score, 12)
        self.assertEqual(self.q.id, 1)

    def test_non_existent_question(self):
        self.assertRaises(LookupError, self.data.get_question, 2)


class TestQuestionSet(unittest.TestCase):
    def setUp(self):
        self.data = DataObjectSource(test_db)

    def test_length(self):
        question_set = self.data.get_questions_asked_by_user(2)
        self.assertEqual(len(question_set), 2)
